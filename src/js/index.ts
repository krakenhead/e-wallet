import forEachPolyfill from './components/forEachPolyfill';
import modal from './components/modal';

document.addEventListener(
  'DOMContentLoaded',
  () => {
    modal();
    forEachPolyfill()
  },
  false
)
