import wait from './wait';

const modal = () => {
  const modalTriggers = document.querySelectorAll('.js-modal') as NodeListOf<HTMLElement>;

  modalTriggers.forEach((modalTrigger:HTMLElement) => {
    modalTrigger.addEventListener('click', handleClick);
  });


  function handleClick(e:any) {
    const modalTarget = (e.target as any).dataset.modalTarget;
    const modalEl = document.querySelector(`.modal[data-modal-name="${modalTarget}"]`) as HTMLElement;
    const modalBody = modalEl.querySelector('.modal__content') as HTMLElement;
    const close = modalEl.querySelector('.js-modal-close') as HTMLElement;

    modalEl.classList.add('show');
    wait(200).then(() => {
      modalBody.classList.add('active');
    });

    close.addEventListener('click', function(e) {
      modalBody.classList.remove('active');
      wait(200).then(() => {
        modalEl.classList.remove('show');
      });
    });
  }
}

export default modal;