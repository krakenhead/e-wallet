<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  include './layout/header.php';
  session_start();
  ?>
</head>

<body>
  <div class="login">
    <div class="login__wrapper">
      <div class="login__header">
        <div class="login__logo"><img src="./src/img/logo.png" alt="Logo"></div>
        <form class="form register__form" method="POST">
          <div class="form__control">
            <label class="form__label">Mobile</label>
            <div class="form__input-wrapper">
              <input class="form__input" type="text" name="mobile" id="mobile" required>
            </div>
          </div>
          <div class="form__control">
            <label class="form__label">Password</label>
            <div class="form__input-wrapper">
              <input class="form__input" type="password" name="password" id="password" required>
            </div>
          </div>
          <div class="form__control">
            <label class="form__label">First Name</label>
            <div class="form__input-wrapper">
              <input class="form__input" type="text" name="firstname" id="firstname" required>
            </div>
          </div>
          <div class="form__control">
            <label class="form__label">Middle Name</label>
            <div class="form__input-wrapper">
              <input class="form__input" type="text" name="middlename" id="middlename">
            </div>
          </div>
          <div class="form__control">
            <label class="form__label">Last Name</label>
            <div class="form__input-wrapper">
              <input class="form__input" type="text" name="lastname" id="lastname" required>
            </div>
          </div>
          <div class="form__submit">
            <input type="submit" value="Register">
          </div>
        </form>
      </div>
    </div>
  </div>
  <?php
  include './layout/core-scripts.php';
  ?>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
  <script>
    const regForm = document.querySelector('.register__form');

    async function handleSubmit(e) {
      e.preventDefault();

      let mobile = e.target.elements.mobile.value.trim();
      let password = e.target.elements.password.value.trim();
      let firstName = e.target.elements.firstname.value.trim();
      let middleName = e.target.elements.middlename.value.trim();
      let lastName = e.target.elements.lastname.value.trim();
      let userObj = {
        mobile,
        password,
        firstName,
        middleName,
        lastName
      };
      let fd = new FormData();

      fd.append('mobile', mobile);
      fd.append('password', password);
      fd.append('firstName', firstName);
      fd.append('middleName', middleName);
      fd.append('lastName', lastName);

      let res = await axios.post('./controller/register.php', fd);
      let { title, icon, message } = res.data;

      if (icon === 'success') {
        Swal.fire({
          title: title,
          icon: icon,
          text: message
        }).then((res) => {
          if (res.isConfirmed) {
            console.log('first')
            location.href = '/e-wallet/login.php';
          }
        });
      } else {
        Swal.fire({
          title: title,
          icon: icon,
          text: message
        });
      }
    }
    regForm.addEventListener('submit', handleSubmit);
  </script>
</body>

</html>