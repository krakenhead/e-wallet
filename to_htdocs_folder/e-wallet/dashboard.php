<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  include './layout/header.php';
  session_start();

  if (empty($_SESSION['loggedIn'])) {
    header('Location: /login.php');
    exit();
  }
  ?>
  <!-- localize variables -->
  <script>
    const accountId = "<?php echo $_SESSION['account_id']; ?>";
    const userMobile = "<?php echo $_SESSION['username']; ?>";
    const userName = "<?php echo $_SESSION['name']; ?>";
  </script>
</head>

<body class="b-flex">
  <?php
  include './layout/sidenav.php';
  ?>
  <main class="content">
    <header class="header">
      <div class="header__button-wrapper"><a class="header__button logout" href="./controller/logout.php">Logout</a></div>
    </header>
    <div class="content__wrapper">
      <h2 class="title">Balance:</h2>
      <p class="balance"> </p>
      <div class="button-wrapper button-wrapper--cta">
        <button class="button button--send js-modal" data-modal-target="send">Send Money</button>
      </div>
      <h2 class="title mt-40 mb-10">Recent transactions</h2>
      <div class="transaction__list">
      </div>
    </div>
  </main>
  <?php
  include './layout/modal-send.php';
  include './layout/core-scripts.php';
  ?>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
  <script>
    let balance;
    async function fetchBalance() {
      const balanceHook = document.querySelector('.balance');
      let fd = new FormData();
      fd.append('accountId', accountId);

      let balanceReq = await axios.post('./controller/getBalance.php', fd);
      balance = +balanceReq.data;

      balanceHook.innerHTML = `$${balance}`;
    }
    fetchBalance();

    const transactionsHook = document.querySelector('.transaction__list');
    async function fetchTransactions() {
      let fd = new FormData();
      fd.append('accountId', accountId);
      fd.append('limit', 5);
      let res = await axios.post('./controller/getTransactions.php', fd);
      let data = res.data;
      console.log(data);

      if (data.transactions) {
        transactionsHook.innerHTML = data.transactions.map(transaction => {
          let amount = transaction.from !== userName ? `+${transaction.amount}` : `-${transaction.amount}`;
          return `
          <div class="transaction__item">
            <div class="transaction__block">
              <p class="transaction__type">${transaction.from !== userName ? 'Received money from' : 'Sent money to'}</p>
              <p class="transaction__user">${transaction.from !== userName ? transaction.from : transaction.to}</p>
            </div>
            <div class="transaction__block">
              <p class="transaction__date">${transaction.timestamp}</p>
              <p class="transaction__cost ${transaction.from !== userName ? 'add' : 'sent'}">${amount}</p>
            </div>
          </div>
          `;
        }).join('');
      } else {
        transactionsHook.innerHTML = `<h3>No Transactions Found</h3>`;
      }
    }
    fetchTransactions();

    const frmSendMoney = document.querySelector('.js-send-money');

    async function handleSubmit(e) {
      e.preventDefault();

      let mobile = e.target.elements.mobile.value.trim();
      let amount = e.target.elements.amount.value.trim();
      let fd = new FormData();

      fd.append('recipient', mobile);
      fd.append('amount', amount);
      fd.append('sender', accountId);
      fd.append('currentBalance', balance);

      if (balance !== 0 && balance > 0) {
        if (mobile !== userMobile) {
          if (amount !== 0) {
            if (!(amount > balance)) {
              let newBal = balance - amount;
              fd.append('newBal', newBal);

              let res = await axios.post('./controller/sendMoney.php', fd);
              let {
                icon,
                title,
                message
              } = res.data;

              if (icon === 'success') {
                Swal.fire({
                    icon: icon,
                    text: message,
                    title: title
                  })
                  .then((res) => {
                    if (res.isConfirmed) {
                      location.reload();
                    }
                  });
              } else {
                Swal.fire({
                  icon: icon,
                  text: message,
                  title: title
                });
              }
            } else {
              Swal.fire({
                icon: 'warning',
                text: 'Insufficient funds!'
              });
            }
          } else {
            Swal.fire({
              icon: 'warning',
              text: 'Please enter valid amount (must be greater than zero)'
            });
          }
        } else {
          Swal.fire({
            icon: 'error',
            text: 'You cannot send money to your own account!'
          });
        }
      } else {
        Swal.fire({
          icon: 'warning',
          text: 'Insufficient funds!'
        });
      }
    }

    frmSendMoney.addEventListener('submit', handleSubmit);
  </script>
</body>

</html>