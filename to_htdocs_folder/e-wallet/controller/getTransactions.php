<?php
require '../config/db.php';
require '../config/sanitize.php';
session_start();

if (!empty($_POST['accountId'])) {
  $accountId = $_POST['accountId'];
  $limit = !empty($_POST['limit']) ? $_POST['limit'] : null;
  if ($limit !== null) {
    $getTransactions = mysqli_query($conn, " SELECT * FROM transact WHERE from_act_id = $accountId OR to_act_id = $accountId ORDER BY `timestamp` DESC LIMIT 5");
  } else {
    $getTransactions = mysqli_query($conn, " SELECT * FROM transact WHERE from_act_id = $accountId OR to_act_id = $accountId ORDER BY `timestamp` DESC");
  }
  $arrRes = [];

  if (mysqli_num_rows($getTransactions) > 0) {
    while ($row = mysqli_fetch_assoc($getTransactions)) {
      $fromUserId = $row['from_act_id'];
      $toUserId = $row['to_act_id'];
      $amount = $row['amount'];
      $timestamp = $row['timestamp'];

      $getFromUser = mysqli_query($conn, " SELECT CONCAT(first_name, ' ', middle_name, ' ', last_name) as name FROM account WHERE account_id = $fromUserId ");
      $getToUser = mysqli_query($conn, " SELECT CONCAT(first_name, ' ', middle_name, ' ', last_name) as name FROM account WHERE account_id = $toUserId ");
  
      $fromName = mysqli_fetch_assoc($getFromUser)['name'];
      $toName = mysqli_fetch_assoc($getToUser)['name'];
  
      $res = array(
        'amount' => $amount,
        'timestamp' => $timestamp,
        'from' => $fromName,
        'to' => $toName
      );
      array_push($arrRes, $res);
    }
    echo json_encode(array(
      'transactions' => $arrRes
    ));
  } else {
    echo json_encode(array(
      'message' => 'No transactions found.'
    ));
  }
}