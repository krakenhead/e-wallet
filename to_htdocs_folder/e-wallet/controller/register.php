<?php
require '../config/db.php';
require '../config/sanitize.php';

if (!empty($_POST['mobile'] && !empty($_POST['password'])) && !empty($_POST['firstName']) && !empty($_POST['lastName']) ) {
  $mobile = sanitize(mysqli_real_escape_string($conn, $_POST['mobile']));
  $password = sanitize(mysqli_real_escape_string($conn, $_POST['password']));
  $firstName = sanitize(ucwords(mysqli_real_escape_string($conn, $_POST['firstName'])));
  $middleName = sanitize(ucwords(mysqli_real_escape_string($conn, $_POST['middleName'])));
  $lastName = sanitize(ucwords(mysqli_real_escape_string($conn, $_POST['lastName'])));

  $checkMobile = mysqli_query($conn, " SELECT * FROM user WHERE username = $mobile ");

  // if mobile does not exist yet
  if (!mysqli_num_rows($checkMobile) > 0) {
    $passwordHashed = password_hash($password, PASSWORD_DEFAULT); // Hash password

    addUser($conn, $mobile, $passwordHashed, $firstName, $middleName, $lastName);
  } else {
    echo json_encode(array(
      'icon' => 'error',
      'message' => 'The mobile number is already registered!',
      'title' => 'Error!'
    ));
  }
} else {
  echo json_encode(array(
    'icon' => 'error',
    'message' => 'Please fill out all the required fields!',
    'title' => 'Error!'
  ));
}

function addUser($conn, $mobile, $passwordHashed, $firstName, $middleName, $lastName) {
  $addUser = mysqli_query($conn, " INSERT INTO user(`username`, `password`) VALUES ('$mobile', '$passwordHashed') ");

  if ($addUser) {
    $getNewUserId = mysqli_query($conn, " SELECT `user_id` FROM user WHERE username = $mobile ");

    while($row = mysqli_fetch_assoc($getNewUserId)) {
      $newUserId = $row['user_id'];
    }
  } else {
    $newUserId = false;
    echo json_encode(array(
      'title' => 'Error!',
      'message' => 'Something went wrong!',
      'icon' => 'error'
    ));
  }

  if ($newUserId !== false) {
    $addAccount = mysqli_query($conn, " INSERT INTO account(first_name, middle_name, last_name, `user_id`, mobile) VALUES ('$firstName', '$middleName', '$lastName', $newUserId, '$mobile') ");
    
    if ($addAccount) {
      $getAccountId = mysqli_query($conn, " SELECT account_id FROM account WHERE `user_id` = $newUserId ");
      $accountId = mysqli_fetch_assoc($getAccountId)['account_id'];
      $addWallet = mysqli_query($conn, " INSERT INTO wallet(balance, account_id) VALUES (0, $accountId) ");
      echo json_encode(array(
        'title' => 'Success!',
        'message' => 'Successfully added account!',
        'icon' => 'success'
      ));
    } else {
      echo json_encode(array(
        'title' => 'Error!',
        'message' => 'Something went wrong! Cannot add account.',
        'icon' => 'error'
      ));
    }
  } else {
    echo json_encode(array(
      'title' => 'Error!',
      'message' => 'Something went wrong! Failed to create user.',
      'icon' => 'error'
    ));
  }
}