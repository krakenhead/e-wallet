<?php
require '../config/db.php';
require '../config/sanitize.php';
session_start();

if (!empty($_POST['recipient']) && !empty($_POST['amount']) && !empty($_POST['sender']) && !empty($_POST['currentBalance']) && !empty($_POST['newBal'])) {
  $recipient = $_POST['recipient'];
  $sender = $_POST['sender'];
  $amount = $_POST['amount'];
  $currentBal = $_POST['currentBalance'];
  $newBal = $_POST['newBal'];

  $getRecipientAccountId = mysqli_query($conn, " SELECT * FROM user us 
  JOIN account ac ON us.user_id = ac.user_id 
  JOIN wallet wa ON wa.account_id = ac.account_id
  WHERE username = '$recipient' ");

  // if recipient exists
  if (mysqli_num_rows($getRecipientAccountId) > 0) {
    while ($row = mysqli_fetch_assoc($getRecipientAccountId)) {
      $recipientAccountId = $row['account_id'];
      $recipientBalance = $row['balance'];
      $newRecipientBalance = $recipientBalance + $amount;
    }

    $updateRecipientBalance = mysqli_query($conn, " UPDATE wallet SET balance = $newRecipientBalance WHERE account_id = $recipientAccountId ");
    $updateSenderBalance = mysqli_query($conn, " UPDATE wallet SET balance = $newBal WHERE account_id = $sender ");
    $insertTransaction = mysqli_query($conn, " INSERT INTO transact(from_act_id, to_act_id, amount) VALUES ($sender, $recipientAccountId, $amount) ");

    if ($updateRecipientBalance && $updateSenderBalance) {
      echo json_encode(array(
        'icon' => 'success',
        'message' => 'Money sent successfully!',
        'title' => 'Success!'
      ));
    } else {
      echo json_encode(array(
        'icon' => 'error',
        'message' => "Error sending money!",
        'title' => 'Error!'
      ));
    }
  } else {
    echo json_encode(array(
      'icon' => 'error',
      'message' => "Recipient doesn't exist!",
      'title' => 'Error!'
    ));
  }
} else {
  echo json_encode(array(
    'icon' => 'error',
    'message' => "Please fill out all the fields!",
    'title' => 'Error!'
  ));
}