<?php
require '../config/db.php';
session_start();

if (!empty($_POST)) {
  $mobile = $_POST['mobile'];
  $password = $_POST['password'];

  $checkUser = mysqli_query(
    $conn,
    "SELECT * FROM user us JOIN account ac
    ON us.user_id = ac.user_id
    WHERE us.username = '$mobile' "
  );

  if (mysqli_num_rows($checkUser) > 0) {
    while ($row = mysqli_fetch_assoc($checkUser)) {
      $passwordHashed = $row['password'];
      $name = $row['first_name'] . ' ' . substr(ucwords($row['middle_name']), 0) . ' ' . $row['last_name'];
      $account_id = $row['account_id'];
      $username = $row['username'];
      $image = $row['imageDir'];
    }

    if (password_verify($password, $passwordHashed)) {
      $_SESSION['name'] = $name;
      $_SESSION['account_id'] = $account_id;
      $_SESSION['username'] = $username;
      $_SESSION['loggedIn'] = true;

      echo 'Passed';
    } else {
      echo 'Password incorrect!';
    }
  } else {
    echo "User doesn't exist!";
  }
} else {
  echo json_encode(array(
    'message' => 'Please fill out the form!',
    'title' => 'Error!',
    'icon' => 'error'
  ));
}