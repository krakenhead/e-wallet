<?php
require '../config/db.php';

session_start();
session_destroy();
header('Location: /e-wallet/login.php');
exit;