<?php
session_start();

if (empty($_SESSION['loggedIn'])) {
  header('Location: /login.php');
  exit();
} else {
  header('Location: /dashboard.php');
}
?>