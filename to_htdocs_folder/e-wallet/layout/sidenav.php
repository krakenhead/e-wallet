<sidenav class="sidenav">
  <h1 class="sidenav__logo"><a href="#"><img src="./src/img/logo.png" alt="Logo"></a>
    <p>e-Wallet</p>
  </h1>
  <div class="sidenav__user">
    <div class="sidenav__user-wrap">
      <div class="sidenav__user-image"><img src="./src/img/default-user-image.jpg" alt=""></div>
      <p class="sidenav__user-name"><?php echo $_SESSION['name']; ?><br><?php echo $_SESSION['username']; ?></p>
    </div>
  </div>
  <nav class="sidenav__nav">
    <ul class="sidenav__nav-list">
      <li class="sidenav__nav-item active"><a class="sidenav__nav-link" href="#">Dashboard</a></li>
      <li class="sidenav__nav-item"><a class="sidenav__nav-link" href="#">Transactions</a></li>
    </ul>
  </nav>
</sidenav>