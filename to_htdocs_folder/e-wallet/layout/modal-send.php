<div class="modal" data-modal-name="send">
  <div class="modal__content">
    <div class="modal__header">
      <h2 class="modal__title">Send Money</h2>
      <button class="modal__close js-modal-close">X</button>
    </div>
    <div class="modal__body">
      <form class="form js-send-money">
        <div class="form__control">
          <label class="form__label">Mobile Number</label>
          <div class="form__input-wrapper">
            <input class="form__input" type="text" name="mobile" id="mobile" required>
          </div>
        </div>
        <div class="form__control">
          <label class="form__label">Amount</label>
          <div class="form__input-wrapper">
            <input class="form__input" type="text" name="amount" id="amount" required>
          </div>
        </div>
        <div class="form__submit">
          <input type="submit" value="Send">
        </div>
      </form>
    </div>
  </div>
</div>