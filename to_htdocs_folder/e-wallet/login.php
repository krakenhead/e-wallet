<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  include './layout/header.php';
  session_start();

  if (!empty($_SESSION['loggedIn'])) {
    header('Location: ./dashboard.php ');
    exit;
  }
  ?>
</head>

<body>

  <div class="login">
    <div class="login__wrapper">
      <div class="login__header">
        <div class="login__logo"><img src="./src/img/logo.png" alt="Logo"></div>
        <form class="form login__form" method="POST">
          <div class="form__control">
            <label class="form__label" for="mobile">Mobile Number</label>
            <div class="form__input-wrapper">
              <input class="form__input" type="text" name="mobile" id="mobile" required>
            </div>
          </div>
          <div class="form__control">
            <label class="form__label" for="username">Password</label>
            <div class="form__input-wrapper">
              <input class="form__input" type="password" name="password" id="password" required>
            </div>
          </div>
          <div class="form__submit">
            <input type="submit" value="Log in">
          </div>
          <p class="form__register form__register--link">Don't have an account yet? <a href="./register.php">Register</a></p>
        </form>
      </div>
    </div>
  </div>
  <?php
  include './layout/core-scripts.php';
  ?>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
  <script>
    const loginForm = document.querySelector('.login__form');

    async function handleSubmit(e) {
      e.preventDefault();

      let mobile = e.target.elements.mobile.value;
      let password = e.target.elements.password.value;
      let fd = new FormData();

      fd.append('mobile', mobile);
      fd.append('password', password);

      let res = await axios.post('./controller/login.php', fd);
      let data = res.data;

      if (data === "Passed") {
        window.location.href = './dashboard.php';
      } else if (data === 'Password incorrect!') {
        Swal.fire({
          icon: "error",
          text: data
        });
      } else if (data === "User doesn't exist!") {
        Swal.fire({
          icon: "error",
          text: data
        });
      } else {
        Swal.fire({
          text: data
        });
      }
    }
    loginForm.addEventListener('submit', handleSubmit);
  </script>
</body>

</html>